#!/bin/bash

say() {
    local text="$1"; shift
    while [ "$1" != "" ]; do
	[ "$1" == "i" ] && local i=$1;
	[ "$1" == "nlb" ] && local nlb=$1;
	[ "$1" == "f" ] && local f=$1;
	[ "$1" == "l" ] && local l=$1;
	shift
    done

    [ ! -x "$(command -v lolcat)" ] && [ ! -x "$(command -v /usr/games/lolcat)" ] && local l=""
    [ ! -x "$(command -v figlet)" ] && local f=""

    [ "x$f" == "xf" ] && f() { figlet -t; } || f() { tee ; }
    [ "x$l" == "xl" ] && l() { /usr/games/lolcat ; } || l() { tee ; }
    
    echo ${i:+-e} ${nlb:+-n} "$text" | f | l
}

exitOrRestart() {
    read -rsN1 -p "Press ENTER to restart or any key to exit." answer
    [ $answer ] || [[ "$answer" =~ [[:blank:]] ]] && exit

    clear && $0 $@
    exit
}

terminate () {
    say "Terminating...\n" i >&2
    exit $1
}

alertUser() {
    local soundFile="/usr/share/sounds/gnome/default/alerts/hum.ogg"
    local text="$1"; shift
    while [ "$1" != "" ]; do
	[ "$1" == "s" ] && local s=true;
	[ "$1" == "test" ] && local test=true;
	[ "$1" == "w" ] && local w=true;
	[ "$1" == "n" ] && local n=true;
	[ ${1:0:2} == "he" ] && local he=${1:2}
	[ ${1:0:2} == "wi" ] && local wi=${1:2}
	shift
    done

    if [ ! -e $soundFile ]; then
	say "Sound file $soundFile does not exits or is not readable.\n" i >&2
	exit 1
    fi

    [ "x$test" == "xtrue" ] && return
    
    [ "$s" ] && mpv $soundFile --no-terminal
    zenity ${w:+--warning} ${n:+--notification} ${he:+--height=$he} ${wi:+--width=$wi} --text "$text"
}

spinner() # http://fitnr.com/showing-a-bash-spinner.html
{
    if [ -z $1 ]; then
	say "The spinner function needs the PID of the last job run in the background as it's first argument." >&2
	say "Maybe the script author forgot '&' or '\$!' ? Just saying." >&2
	exit 1
    fi    

    local pid=$1; local delay=0.5; local spinstr='|/-\'

    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "$spinstr"
        local spinstr=$temp${spinstr%"$temp"}
        sleep $delay
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}

sourceFile () {
    [ ! -r "$1" ] && say "$1 should exists or be readable." >&2 && terminate 2
    . $1
}

checkSetting_File () {
    local setting="$1"; local name="$2"
    if [ -z "$setting" ] || [ ! -e "$setting" ] || [ ! -r "$setting" ]; then
	say "The file or directory the '$name' variable(s) point to does not exist or is not readable.\n" i >&2
	say "The variable(s) content is: '$setting'." >&2
	terminate 1
    fi
}

installFromRepo () {
    local gitUrl=$1; installDir=$2; local pullSuccess;

    [ $EUID -ne 0 ] && echo "You must be root to use this 'installFromRepo'." >&2 && exit 1
    ! command -v git &> /dev/null && echo "Please install git first." >&2 && exit 2

    [[ -z $installDir || -z $gitUrl ]] && echo "InstallDir and gitUrl arguments must be provided." >&2 && exit 2

    [ ! -d "$installDir" ] && mkdir "$installDir"

    [[ -z "$(ls -A $installDir)" ]] && git -C "$installDir" clone "$gitUrl" .

    git -C "$installDir" status &> /dev/null &&	git -C "$installDir" pull || pullSuccess=false

    if [ "$pullSuccess" = "false" ]; then
	read -p "Something went wrong. Would you like to DELETE ALL FILES in '$installDir' and clone again ? [y/N]." ans
	[ "$ans" != y ] && terminate 2
	if [ "$ans" = y ]; then
	    rm -rf "${installDir:?}/"* "${installDir:?}/".*
	    git -C "$installDir" clone "$gitUrl" .
	fi
    fi

    if [[ -n "$(ls -A $installDir)" ]] && ! git -C "$installDir" status &> /dev/null
    then
	echo "Not a git repo but file(s) already exists in $installDir." >&2
	echo "Check what's wrong please." >&2
	echo "Terminating" >&2 && exit 2
    fi

    rm -f "$installDir/.projectile"
    rm -f "$installDir/todo.org"

    echo -e "Installation successful. Happy hacking!\n"
}

checkFunctionArgs() {
    for arg in "$@"; do
	if [ -z	"$arg" ]; then
	    say "Mandatory argument missing for ${FUNCNAME} function. This is a bug somewhere." >&2
	    terminate 2
	fi
    done
}

# #+ Mode normal
# ResetColor="$(tput sgr0)"
# bold=$(tput smso) # "Surligné" (bold)
# offbold=$(tput rmso) # "Non-Surligné" (offbold)

# # Couleurs (gras)
# Red="$(tput bold ; tput setaf 1)"
# Green="$(tput bold ; tput setaf 2)"
# Yellow="$(tput bold ; tput setaf 3)"
# Blue="$(tput bold ; tput setaf 4)"
# BlueCyan="$(tput bold ; tput setaf 6)"


# tput cup 0 2 clear # Adressage du curseur ligne 0 colonne 2
# echo "Entrez les informations demandées dans le champ ayant le curseur."
# tput cup 1 2
# echo "Appuyez sur Entrée pour passer au champ suivant."
# tput cup 3 30
# echo "${bold}Questions/Réponses${offbold}"

# # Pré-affichage des champs
# tput cup 5 5
# echo -e "Nom : \c"
# tput cup 7 5
# echo -e "Prénom : \c"
# tput cup 9 5
# echo -e "Age : \c"

# # Pré-affichage des données
# tput cup 12 2
# echo -e "Votre nom est : "
# tput cup 13 2
# echo -e "Votre prénom est : "
# tput cup 14 2
# echo -e "Vous avez  ans. "

# #### Interaction du script ####
# tput cup 5 5 # Adressage du curseur ligne 5 colonne 5
# echo -e "Nom : \c"
# read nom
# tput cup 7 5
# echo -e "Prénom : \c"
# read prenom
# tput cup 9 5
# echo -e "Age : \c"
# read age

# #### Affichage des réponses ####
# tput cup 12 2
# echo -e "${Green}Votre nom est : ${ResetColor}"${Red}$nom${ResetColor}
# tput cup 13 2
# echo -e "${Green}Votre prénom est : ${ResetColor}"${BlueCyan}$prenom${ResetColor}
# tput cup 14 2
# echo -e "${Green}Vous avez ${Yellow}$age ${Green}ans. ${ResetColor}"
# tput cup 20 0
